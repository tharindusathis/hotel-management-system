from flask import *
from project import db
from project.model import Users,Handler, Rooms, Room_allotted, Drafts,Hotels,Facilities
from project.forms import UserLoginForm, RegisterForm, RoomBookForm, checkOutForm
from flask_login import login_user, current_user, logout_user, login_required
from project.picture_handler import add_profile_pic
import stripe
import os
from project.script import name,address 
from sqlalchemy import and_
from datetime import datetime
from werkzeug.urls import url_parse
# print(hotel_info)

admin = Blueprint('admin',__name__)

stripe_keys = {
  'publishable_key': "pk_test_EzB9rTKHvSb9uZwjzbv8q6kz00tdsUMFq0",
  'secret_key': "sk_test_SHwhbYRWt8S9ff2PfHhLthwq00M7u3Yppv"
}

stripe.api_key = stripe_keys['secret_key']

@admin.route('/')
def index():
    # db.drop_all()
    # db.create_all()
    return render_template('index.html')

# users area #####################################################################

@admin.route('/logout', methods=['GET', 'POST'])
def logout():
    logout_user()
    return redirect(url_for('admin.index'))

def facilities_function():
    x = Facilities.query.all()
    L = []
    for x in x :
        facilities = {}
        if x.gym is True:
            facilities['gym'] = True
        if x.FoodBeverages is True:
            facilities['FoodBeverages'] = True
        if x.Parking is True:
            facilities['Parking'] = True  
        if x.Tv is True:
            facilities['Tv'] = True
        if x.wifi is True:
            facilities['wifi'] = True 
        L.append(facilities)  
    return L

 
@admin.route('/signup', methods=['GET', 'POST'])
def signup():
    # if current_user.is_authenticated:
        # return redirect(url_for('admin.index'))
    form = RegisterForm()
    if form.validate_on_submit():
        user = Users(userId=form.userId.data, name=form.name.data,password = form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('admin.index'))
    return render_template('signup.html', title='Register', form=form)
 

@admin.route('/userlogin', methods=['GET', 'POST'])
def UserLogin():
    # if current_user.is_authenticated:
        # return redirect(url_for('admin.index'))
    form = UserLoginForm()
    if form.validate_on_submit():
        user = Users.query.filter_by(userId = form.userId.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid Credentials')
            return render_template('userlogin.html',form=form)
        # flash('Logged in successfully.')
        login_user(user)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('admin.index')
        return redirect(next_page)
         # return render_template(url_for('admin.UserLogin'))
    return render_template('userlogin.html', form=form)


 
data = {}
  
@admin.route('/userDashboard', methods=['GET', 'POST'])
def userDashboard():
    # Rooms.query.filter_by(roomNo = roomNo, hotelId = hotelId).first().status = False
    # Room_allotted.query.filter_by(roomNo = roomNo, hotelId = hotelId).first().paymentStatus = False    
    # db.session.commit()
    x = Users.query.filter_by(userId = current_user.userId).first()
    if x is not None:
        print(x.name)
    y = drafts = Drafts.query.filter_by(userId = current_user.userId).first()
    if y is not None:
        print(y.hotelId)
    # for each in drafts:
    #     for x in each:
    #         print(x.userId,x.hotelId,x.roomNo)

    return render_template('userDashboard.html')
 

@admin.route('/adminlogin', methods=['GET', 'POST'])
def adminLogin():
    # if current_user.is_authenticated:
        # return redirect(url_for('admin.index'))
    form = UserLoginForm()
    print('ok')
    if form.validate_on_submit():
        administrator = Handler.query.filter_by(adminId = form.userId.data).first()
        if administrator is not None and  administrator.check_password(form.password.data):
            login_user(administrator)
            return render_template('adminDashboard.html',form=form)
            
        flash('Invalid Credentials')
        # next_page = request.args.get('next')
        # if not next_page or url_parse(next_page).netloc != '':
            # next_page = url_for('admin.index')
        # return redirect(next_page)
        # return render_template(url_for('admin.UserLogin'))
    return render_template('adminLogin.html',form=form)

@admin.route('/adminDashboard', methods=['GET', 'POST'])
def adminDashboard():
    return render_template('adminDashboard.html')
   

################# Recommendation Funciton Implemented Here ################
from recommendation import similarity_hotel
def recommendation_function():
    alloted = Room_allotted.query.filter_by(userId = current_user.userId)
    booked_hotel = []
    if alloted is not None:
        for each in alloted:
            if each.hotelId not in booked_hotel:
                booked_hotel.append(each.hotelId)
    # print(booked_hotel)


    print(similarity_hotel[1][1])
    # print(similarity_hotel.shape)
    new_recommendation = []
    for each in booked_hotel:
        hotelNo = ''.join(filter(lambda i: i.isdigit(),each))
        # print(hotelNo)
        hotelNo = int(hotelNo)

        for i in range(similarity_hotel.shape[1]): # finding the hotels similar to the ones booked by user using hotel similarity matrix
            x = similarity_hotel[hotelNo][i]
            if x >= 0.94 :
                new_recommendation.append(i)
    # print(new_recommendation)
    return new_recommendation 
 