# Hotel-Management-System-QE-Project
General instructions for **QE Team** members of **Hotel Management System** . 

* Cloning;
```shell
 git clone https://gitlab.com/tharindusathis/hotel-management-system
 cd hotel-management-system
```

* Create a virtual environment as:
```shell
python -m venv env
```


Then, activate python virtual env before working on project
* if you are using CMD:
```shell
 ./env/Scripts/activate
```
* if you are using git-bash:
```shell
 . ./env/Scripts/activate
```
 
* The install requirements as:
```shell
pip install -r requirements.txt
```

* Then, before starting server;
```shell
change path in project/scripts.py to 
the path should be relative path to your data.json file
```
* Then, for start server;
```shell
FLASK_ENV=development
flask run
```

# Before commiting your changes remember to run `git pull` to sync the repo with master!

